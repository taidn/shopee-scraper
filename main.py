import json
import time
from datetime import datetime

from with_selenium import browse_with_selenium as sel

SHOPEE_URL: str = "https://shopee.vn/"
CATEGORY_ENDPOINT: str = "api/v2/category_list/get"
DEEP_LEVEL: int = 100  # Number of page to browse
SAVED_FILE: str = "scraped_urls.json"
DIR_PATH: str = "data/scrape_history/"


def main():
    """Using selenium to scrape Shopee """
    scraped_urls = []
    start_time: int = current_milli_time()

    driver = sel.init()
    urls = sel.build_url_categories(driver=driver,
                                    web_url=SHOPEE_URL,
                                    category_endpoint=CATEGORY_ENDPOINT)
    for url in urls:
        scraped_url = sel.browser(driver=driver, url=url, deep_level=DEEP_LEVEL)
        scraped_urls.append(scraped_url)
        print(scraped_url)
    save_history(scraped_urls, start_time)
    driver.close()


def save_history(scraped_urls: list, start_time: int):
    today: str = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
    exec_time: int = current_milli_time() - start_time
    file_name: str = "[" + today + "] " + "[" + str(exec_time) + "] " + SAVED_FILE
    with open(DIR_PATH + file_name, 'w', encoding='utf-8') as f:
        json.dump(scraped_urls, f, ensure_ascii=False, indent=4)


def current_milli_time():
    return round(time.time())


if __name__ == "__main__":
    main()
