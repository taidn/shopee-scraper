# Scrape-product

### Run command below before running the main python file, this command will install all the requirements to run the program.

```console
foo@bar:~$ pip install -r requirements.txt
```

### Command below used to run the program.

```console
foo@bar:~$ python3 main.py
```