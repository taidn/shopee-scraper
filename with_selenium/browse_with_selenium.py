import time

import requests
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.webdriver import WebDriver
from slugify import slugify


def init():
    """Scrape-id using Firefox webdriver as a default, 
    here is the list of all supported webdriver
    
    https://www.selenium.dev/documentation/en/getting_started_with_webdriver/browsers/
    https://www.selenium.dev/documentation/en/getting_started_with_webdriver/third_party_drivers_and_plugins/
    
    driver = webdriver.Firefox() 
    """

    driver = webdriver.Firefox()
    driver.implicitly_wait(10)
    return driver


def build_url_categories(driver: WebDriver, web_url: str, category_endpoint: str):
    """This method build categories's url
    """
    urls = []
    headers = {
        'authority': 'shopee.vn',
        'x-shopee-language': 'vi',
        'x-requested-with': 'XMLHttpRequest',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36 Edg/83.0.478.61',
        'x-api-source': 'pc',
        'accept': '*/*',
    }

    params = ()
    try:
        json = requests.get(web_url + category_endpoint, headers=headers, params=params).json()
    except ValueError:
        print("Empty response from", web_url)
        driver.quit()
        exit()

    for category in json['data']['category_list']:
        urls.append(url_category_builder(web_url, category['display_name'], category['catid']))

    return urls


def url_category_builder(web_url, display_name, category_id):
    """This method build a compact url that
    will be use when scraping
    """
    return web_url + slugify(display_name).replace(" ", "-") + "-cat." + str(category_id)


def browser(driver, url, deep_level=5):
    driver.get(url)
    time.sleep(5)

    counter: int = 0
    for i in range(deep_level):
        try:
            continue_button: list[any] = driver.find_elements_by_xpath(
                '/html/body/div[1]/div/div[3]/div/div[4]/div[2]/div/div[1]/div[2]/button[2]')
            if len(continue_button) > 0:
                continue_button[0].click()
            else:
                break
            counter += 1
            time.sleep(2)
        except NoSuchElementException as e:
            print(e)
    return url + " (" + str(counter) + " pages)"
